# Ansible configuration for HTPC #

# Setup #

## Requirements ##

General requirements
* SSH connection to all hosts
* At least one user (root is fine)

### Setup SSH ###

* Generate public and private keys

`ssh-keygen` (interactive)
* Allow passwordless connection from host to all used guests

`ssh-copy-id localhost` (interactive)

### Add password for the vault ###

`echo '$password' > ~/.vault_pass.txt`

### Create a password and put it in the playbook ###
`mkpasswd --method=sha-512`

### Clone the project ###

* `git clone https://gitlab.com/liamesteban/laptop.git`

# Run #

`ansible-playbook local.yml`
* If the command keeps asking for passwords, use

`ssh-agent zsh`
`ssh-add ~/.ssh/id_rsa`
