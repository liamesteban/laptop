#!/bin/bash

echo "Stop the kernel printing messages to the console using printk"
# or add quiet to the kernel command line
sysctl kernel.printk='4 5 1 4'
echo '==> kernel.printk = 4 5 1 4' | sudo tee -a /etc/sysctl.d/printk.conf

echo "Set font Terminus"
echo '==> FONT=Lat2-Terminus16' | sudo tee -a /etc/vconsole.conf

echo "Set the colorscheme to Gruvbox"
curl -o /tmp/console.sh https://raw.githubusercontent.com/programble/dotfiles/fa22c1e9a9ff6aa1e5b40fc75033d3f5611b3ba0/console.sh
chmod +x /tmp/console.sh
/tmp/console.sh > /tmp/issue.new
cat /etc/issue >> /tmp/issue.new
sudo install --mode 644 /tmp/issue.new /etc/issue
# https://github.com/joepvd/tty-solarized/tree/a435d9b4f7aaa9e0dd0494c504fc1fe3c6a22eee
# the script can also be sourced in ${ZDOTDIR:-$HOME}/.zlogin or .profile, but this requires keeping the script somewhere

echo "Stop the cursor blinking"
echo '==> w /sys/class/graphics/fbcon/cursor_blink - - - - 0' | sudo tee -a /etc/tmpfiles.d/cursor_blink.conf
set -g terminal-overrides "linux:cnorm=\e[?25h\e[?8c"

echo "Set CapsLock to Ctrl"
echo '==> keycode 125 = Ctrl' | sudo tee -a /etc/vconsole.conf
echo "Use a compose key for special characters"

echo "Install some extra applications,
     - jfbview :: pdf
     - fbv :: images
     - fbgrab :: screenshots"
